const run = () => {
    const str = 'deed dead moon noon age nun one level sagas sugar peep';

    const words = str.split(" ");
    const res = [];
    for (word of words) {
        if (isPalindrome(word)) {
            res.push(word);
        }
    }
    for (word of res) {
        jsConsole.writeLine(word);
    }

    function isPalindrome(str) {
        return str == str.split('').reverse().join('');
    }
}