const run = () => {
    const text = 'blabla@mail.ua.com  blablabla444 blabla123@mail11.com  blab123labla blagggbla@mail.com  blablabla bla456bla@mail.com  blablabla blabla@gmail.com  blablabla';

    const res = findEmails(text);
    for (r of res) {
        jsConsole.writeLine(r);
    }

    function findEmails(text) {
        const regex = new RegExp("[a-zA-Z0-9\-\.\_]+@[a-z0-9]+(\.[a-z]+)+", "g");
        const res = [];
        for (r of text.matchAll(regex)) {
            res.push(r[0]);
        }
        return res;
    }
}