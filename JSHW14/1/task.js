const addElement = () => {
    const elementValue = document.querySelector('#elements').value;
    const itemsList = document.querySelector('#items');
    const createItem = document.createElement('li');
    itemsList.appendChild(createItem);
    const checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    const textNode = document.createTextNode(elementValue);
    createItem.appendChild(checkbox);
    createItem.appendChild(textNode);
}

const deleteElement = () => {
    const itemsList = document.querySelector('#items');
    const deleteElements = document.getElementsByTagName('li');
    for (let item of deleteElements) {
        const input = item.getElementsByTagName("input")[0];
        if (input.checked) {
            itemsList.removeChild(item);
        }
    }
}
const hideElement = () => {
    const elems = document.getElementsByTagName('li');
    for (let item of elems) {
        const input = item.getElementsByTagName("input")[0];
        if (input.checked) {
            item.hidden = true;
        }
    }
}
const showHiddenElement = () => {
    const elems = document.getElementsByTagName('li');
    for (let item of elems) {
        if (item.hidden) {
            item.hidden = false;
        }
    }
}