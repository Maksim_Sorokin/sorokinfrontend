const run = () => {
    const num = document.querySelector('#str').value;
    const res = fib(num);
    jsConsole.writeLine(res);

    function fib(n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fib(n - 2) + fib(n - 1);
    }
}