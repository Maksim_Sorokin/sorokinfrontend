const run = () => {
    const comp = function(a, b) {
        if (a == b) {
            return 1;
        } else {
            return -1;
        }
    }
    const res1 = comp("abc", "abc");
    jsConsole.writeLine(res1);

    const res2 = comp("abC", "abc");
    jsConsole.writeLine(res2);
}