const run = () => {

    function Person(firstname, lastname, age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = +age;

        Object.defineProperties(this, {
            fullname: {
                get: () => this.firstname + " " + this.lastname,
                set: (fullname) => {
                    const names = fullname.split(' ');
                    this.firstname = names[0];
                    this.lastname = names[1];
                }
            }
        })

        this.introduce = function() {
            const nameRegex = new RegExp("^[a-zA-Z]{3,20}$");
            if (!this.firstname.match(nameRegex)) {
                return "Invalid firstname";
            }
            if (!this.lastname.match(nameRegex)) {
                return "Invalid lastname";
            }
            if (this.age < 0 || this.age > 150) {
                return "Invalid age";
            }
            return "Hello! My name is " + this.fullname + " and I am " + this.age + "-years-old";
        }
    }
    const firstname = document.querySelector("#firstname").value;
    const lastname = document.querySelector("#lastname").value;
    const age = document.querySelector("#age").value;

    const user = new Person(firstname, lastname, age);
    const res = user.introduce();
    jsConsole.writeLine(res);
}