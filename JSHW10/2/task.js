const run = () => {
    const url = document.querySelector("#url").value;
    const res = parseUrl(url);
    jsConsole.writeLine("protocol: " + res.protocol);
    jsConsole.writeLine("server: " + res.server);
    jsConsole.writeLine("resource: " + res.resource);

    function parseUrl(url) {
        const regex = "([a-z]+):\/\/([a-z0-9\-\.]+)([a-z0-9\.\/]+)?";
        const res = url.match(regex)
        return {
            protocol: res[1],
            server: res[2],
            resource: res[3]
        }
    }
}