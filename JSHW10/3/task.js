const run = () => {
    const html = "<p>Please visit <a href=\"http://academy.telerik. com\">our site</a> to choose a training course. Also visit <a href=\"www.devbg.org\">our forum</a> to discuss the courses.</p>"
    const res = replaceTags(html);
    jsConsole.writeLine(res);

    function replaceTags(html) {
        const regex = new RegExp("<a href=([\'\"].+?[\'\"])>(.*?)<\/a>", "g");
        return html.replace(regex, "[URL=$1]$2[/URL]");
    }
}