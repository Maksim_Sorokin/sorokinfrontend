const run = () => {
    const html = document.querySelector("#key").value;
    const res = getRawText(html);
    jsConsole.writeLine(res);

    function getRawText(html) {
        const regex = new RegExp("\<\/?.+?\>", "g")
        return html.replace(regex, "");
    }
}