const papers = document.getElementsByClassName("paper");
const trash = document.getElementById("trash");
let currentPaper = null;

window.addEventListener('mouseup', function() {
    if (currentPaper != null && isIntersect(currentPaper, trash)) {
        document.body.removeChild(currentPaper);
        currentPaper = null;
        updateTrashState();
    }
    currentPaper = null;
}, false);
window.addEventListener('mousemove', function(e) {
    if (currentPaper != null) {
        currentPaper.style.top = e.clientY + 'px';
        currentPaper.style.left = e.clientX + 'px';
        updateTrashState();
    }
}, true);

function updateTrashState() {
    if (currentPaper != null && isIntersect(currentPaper, trash)) {
        trash.src = "trash-opened.jpg";
    } else {
        trash.src = "trash-closed.webp";
    }
}

function isIntersect(paper, trash) {
    const r1 = paper.getBoundingClientRect();
    const r2 = trash.getBoundingClientRect();
    return intersectRect(r1, r2);
}

function intersectRect(r1, r2) {
    return !(r2.left > r1.right ||
        r2.right < r1.left ||
        r2.top > r1.bottom ||
        r2.bottom < r1.top);
}

for (paper of papers) {
    paper.addEventListener('mousedown', function(e) {
        e.preventDefault();
        currentPaper = e.target;
    }, true);
}