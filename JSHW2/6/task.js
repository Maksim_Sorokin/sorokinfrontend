const run = () => {
    const arr = document.querySelector('#str').value.split(' ');
    let res = {};
    for (c of arr) {
        let counter = c in res ? res[c] : 0;
        res[c] = counter + 1;
    }
    let maxCount = 0;
    let maxChar = null;
    for (c of Object.entries(res)) {
        if (c[1] > maxCount) {
            maxCount = c[1];
            maxChar = c[0];
        }
    }
    jsConsole.writeLine(maxChar + ": " + maxCount + " times");
}