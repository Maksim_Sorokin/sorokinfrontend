const run = () => {
    const arr = document.querySelector('#str').value.split(' ');
    const target = document.querySelector('#num').value;

    const res = searchInArray(arr, target, 0);
    jsConsole.writeLine(res >= 0 ? res : "not found");
}

function searchInArray(array, target, startIndex) {
    if (array.length == 0 || (array.length == 1 && array[0] != target)) {
        return -1;
    }
    const middle = Math.floor(array.length / 2);
    const val = array[middle];
    if (target == val) {
        return startIndex + middle;
    } else if (+val > +target) {
        return searchInArray(array.slice(0, middle), target, startIndex);
    } else {
        return searchInArray(array.slice(middle, array.length), target, startIndex + middle);
    }
}