const run = () => {
    const arr = document.querySelector('#str').value.split(' ');
    let maxCounter = 0;
    let maxCharacter = null;
    let curCounter = 0;
    let curChar = arr[0];
    for (current of arr) {
        if (current == curChar) {
            curCounter++;
        } else {
            if (curCounter > maxCounter) {
                maxCounter = curCounter;
                maxCharacter = curChar;
            }
            curChar = current;
            curCounter = 1;
        }

    }
    jsConsole.writeLine(maxCharacter + ": " + maxCounter + " times");
}