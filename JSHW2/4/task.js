const run = () => {
    const arr = document.querySelector('#str').value.split(' ');
    let max = [];
    let cur = [];
    let lastChar = -1;
    for (current of arr) {
        if (+current > lastChar) {
            cur.push(+current);
        } else {
            if (cur.length > max.length) {
                max = cur;
            }
            cur = [];
        }
        lastChar = +current
    }
    jsConsole.writeLine(max);
}