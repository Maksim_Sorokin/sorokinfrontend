const run = () => {
    const text = document.querySelector("#str").value;
    const res = checkSpam(text);
    jsConsole.writeLine(res);

    function checkSpam(s) {
        const t = s.toLowerCase();
        return t.indexOf("spam") != -1 || t.indexOf("sex") != -1;
    }
}