const run = () => {
    const text = document.querySelector("#str").value;
    const res = cut(text);
    jsConsole.writeLine(res);

    function cut(s) {
        if (s.length > 20) {
            return s.slice(0, 20) + "...";
        } else {
            return s;
        }
    }
}