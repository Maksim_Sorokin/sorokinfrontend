const createDivs = () => {
    for (let i = 0; i < 5; i++) {
        const elem = document.createElement('div');
        elem.setAttribute("id", "div" + i);
        elem.className = "animated"
        document.body.appendChild(elem);
    }
    setElementsPosition(0);
}

const animateDivs = () => {
    let angle = 0;
    window.setInterval(function() {
        setElementsPosition(angle);
        angle++;
        if (angle > 360) {
            angle = 0;
        }
    }, 100);
}

function setElementsPosition(angle) {
    const centerX = 400;
    const centerY = 400;
    const radius = 100;
    for (let i = 0; i < 5; i++) {
        const elem = document.getElementById("div" + i);
        elem.style.left = (centerX + radius * Math.cos(i + angle * Math.PI / 180)) + "px";
        elem.style.top = (centerY + radius * Math.sin(i + angle * Math.PI / 180)) + "px";
    }
}