const run = () => {
    const persons = [
        { firstname: "Natalya", lastname: "Osipenko", age: 61 },
        { firstname: "Kristina", lastname: "Osipenko", age: 23 },
        { firstname: "Artem", lastname: "Korhov", age: 25 },
        { firstname: "Artem", lastname: "Seredinskiy", age: 20 },
        { firstname: "Artem", lastname: "Artsiomenka", age: 20 },
        { firstname: "Sergey", lastname: "Osipenko", age: 20 },
        { firstname: "Vinni", lastname: "Puh", age: 15 }

    ]

    function groupBy(persons, key) {
        const res = {};
        for (p of persons) {
            const prop = p[key];
            if (res[prop] !== undefined) {
                res[prop].push(p);
            } else {
                res[prop] = [p];
            }
        }
        return res;
    }

    function processAndWrite(persons, prop) {
        const res = groupBy(persons, prop);
        for (key in res) {
            jsConsole.writeLine(key);
        }
        jsConsole.writeLine("-------------");
    }

    processAndWrite(persons, "firstname");
    processAndWrite(persons, "lastname");
    processAndWrite(persons, "age");
}