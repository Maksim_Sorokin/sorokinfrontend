const run = () => {
    const persons = [
        { firstName: "Maksim", lastName: "Sorokin", age: 26 },
        { firstName: "Anna", lastName: "Sergeenko", age: 25 },
        { firstName: "Alexander", lastName: "Gorbach", age: 26 },
        { firstName: "Ilya", lastName: "Kolyaskin", age: 26 },
        { firstName: "Yaroslava", lastName: "Gorbach", age: 27 }
    ]

    function findYoungestPerson(persons) {
        let youngest = persons[0];
        for (p of persons) {
            if (p.age < youngest.age) {
                youngest = p;
            }
        }
        return youngest;
    }

    const res = findYoungestPerson(persons);
    jsConsole.writeLine(res.firstName + " " + res.lastName);
}