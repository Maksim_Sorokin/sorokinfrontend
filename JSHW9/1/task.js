const run = () => {
    const key = document.querySelector('#key').value;

    const obj = {
        name: 'John',
        age: 30,
        weight: 80,
        height: 170,
    }

    function hasProperty(obj, key) {
        for (k in obj) {
            if (k == key) {
                return true;
            }

        }
        return false;
    }
    const res = hasProperty(obj, key);
    jsConsole.writeLine(res);
}