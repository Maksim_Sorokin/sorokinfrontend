const run = () => {
    const arr = document.querySelector('#arr').value.split(" ");
    const target = document.querySelector('#target').value;
    const count = findOccurrences(arr, target);
    const testRes = test();

    jsConsole.writeLine(count);
    jsConsole.writeLine("test function returned: " + testRes);

    function findOccurrences(arr, target) {
        let count = 0;
        for (i of arr) {
            if (i == target) {
                count++;
            }
        }
        return count;
    }

    function test() {
        return findOccurrences([1, 2, 5, 2, 4, 1, 2], 2) == 3;
    }
}