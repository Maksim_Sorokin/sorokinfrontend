const run = () => {
    const arr = document.querySelector('#arr').value.split(" ");
    const index = document.querySelector('#index').value;
    const res = check(arr, +index);

    jsConsole.writeLine(res);


    function check(arr, index) {
        if (index < 1 || index >= arr.length - 1) {
            return false;
        }
        const elem = +arr[index];
        const prev = +arr[index - 1];
        const next = +arr[index + 1];
        return prev < elem && next < elem;
    }
}