const run = () => {
    const findText = document.querySelector('#abc').value;
    const findWord = document.querySelector('#word').value;
    const res = findOccurrences(findText, findWord, true);
    jsConsole.writeLine(res);

    function findOccurrences(text, word) {
        return findOccurrences(text, word, false);
    }

    function findOccurrences(text, word, ignoreCase) {
        let res = 0;
        let pos = 0;
        if (ignoreCase) {
            text = text.toLowerCase();
            word = word.toLowerCase();
        }
        while (true) {
            let foundPos = text.indexOf(word, pos);
            if (foundPos == -1) break;

            res++;
            pos = foundPos + 1;
        }
        return res;
    }
}