const run = () => {
    const arr = document.querySelector('#arr').value.split(" ");
    const res = findInArray(arr);

    jsConsole.writeLine(res);

    function findInArray(arr) {
        for (i = 0; i < arr.length; i++) {
            if (check(arr, i)) {
                return i;
            }
        }
        return -1;
    }

    function check(arr, index) {
        if (index < 1 || index >= arr.length - 1) {
            return false;
        }
        const elem = +arr[index];
        const prev = +arr[index - 1];
        const next = +arr[index + 1];
        return prev < elem && next < elem;
    }
}