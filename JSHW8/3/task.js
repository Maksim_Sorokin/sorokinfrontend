const run = () => {
    let obj = {
        title: "Pencil",
        count: 20
    };
    const res = clone(obj);
    jsConsole.writeLine(res.title);
    jsConsole.writeLine(res.count);

    function clone(obj) {
        const clone = {};
        for (let key in obj) {
            clone[key] = obj[key];
        }
        return clone;
    }
}