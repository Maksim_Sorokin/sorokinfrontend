const run = () => {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    function Line(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;

        this.getLength = function() {
            return calcDistance(p1, p2);
        }
    }

    function calcDistance(p1, p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    function canBeTriangle(l1, l2, l3) {
        const a = l1.getLength();
        const b = l2.getLength();
        const c = l3.getLength();
        return a + b > c && a + c > b && b + c > a;
    }

    function displayLinesInfo(l1, l2, l3) {
        jsConsole.writeLine("Line 1: length: " + l1.getLength())
        jsConsole.writeLine("Points: (" + l1.p1.x + ", " + l1.p1.y + "), (" + l1.p2.x + ", " + l1.p2.y + ")");

        jsConsole.writeLine("Line 2: length: " + l2.getLength())
        jsConsole.writeLine("Points: (" + l2.p1.x + ", " + l2.p1.y + "), (" + l2.p2.x + ", " + l2.p2.y + ")");

        jsConsole.writeLine("Line 3: length: " + l3.getLength())
        jsConsole.writeLine("Points: (" + l3.p1.x + ", " + l3.p1.y + "), (" + l3.p2.x + ", " + l3.p2.y + ")");

        jsConsole.writeLine("Can create a triangle: " + canBeTriangle(l1, l2, l3));
    }


    const l1 = new Line(new Point(3, 3), new Point(5, 5));
    const l2 = new Line(new Point(2, 2), new Point(4, 4));
    const l3 = new Line(new Point(1, 1), new Point(3, 3));

    displayLinesInfo(l1, l2, l3);

    const l4 = new Line(new Point(3, 3), new Point(5, 5));
    const l5 = new Line(new Point(2, 2), new Point(4, 4));
    const l6 = new Line(new Point(1, 1), new Point(3, 8));

    displayLinesInfo(l4, l5, l6);
}