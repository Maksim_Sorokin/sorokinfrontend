const run = () => {

    const res = stringFormat("text {1} text {0} text {2} text {3} text {4} text {5} text {6} text {7} text {8} text {9} text {10} text {11} text {12} text {13} text {14} text {15} text {16} text {17} text {18} text {19} text {20} text {21} text {22} text {23} text {24} text {25} text {26} text {27} text {28} text {29} text {30}", "alpha", "beta", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30");
    jsConsole.writeLine(res);

    function stringFormat() {
        const regex = new RegExp("\{(\.+?)\}", "g");
        return arguments[0].replace(regex, (a, b) => arguments[+b + 1]);
    }
}