const run = () => {
    function stringFormat(obj, template) {
        let res = template;
        for (field in obj) {
            const regex = new RegExp("\{(" + field + ")\}", "g");
            res = res.replace(regex, obj[field]);
        }
        return res;
    }

    function generateHtmlList(data, template) {
        let str = "<ul>";
        for (item of data) {
            str = str + "<li>" + stringFormat(item, template) + "</li>";
        }
        str = str + "</ul>";
        return str;
    }

    var people = [
        { name: "Шапокляк", age: 55 },
        { name: "Чебурашка", age: 17 },
        { name: "Крыска-Лариска", age: 18 },
        { name: "Крокодильчик", age: 26 },
        { name: "Турист- завтрак крокодильчика", age: 32 }
    ]

    var divPeople = document.getElementById("list-item");
    var template = divPeople.innerHTML;
    var peopleList = generateHtmlList(people, template);
    divPeople.innerHTML = peopleList;


}