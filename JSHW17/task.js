const url = "/data"

function getJSON() {
    fetch(url).then(async function(response){
          data = await response.json();
          document.getElementById("getOutput").innerText = JSON.stringify(data)
      });
}

function postJSON() {
    const firstname = document.getElementById("fname").value;
    const lastname = document.getElementById("lname").value;
    const age = document.getElementById("age").value;
    const person = {
        firstname: firstname,
        lastname: lastname,
        age: age
    }
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(person),
        headers: {'Content-Type': 'application/json'}
    }).then(async function(response){
        data = await response.json();
        document.getElementById("postOutput").innerText = JSON.stringify(data)
    });
}