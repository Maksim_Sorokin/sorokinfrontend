var express = require('express');
var app = express();

const data = require('./data.json')

app.use(require('body-parser').json());
app.use(express.static('.'));

app.get("/data", function(req, res) {
    res.json(data);
})

app.post("/data",  function (req, res) {
    let data = JSON.stringify(req.body);
    res.send(data);
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});