const run = () => {
    const text = document.querySelector('#str').value;
    const res = checkBrackets(text);
    jsConsole.writeLine(res ? "Valid" : "Invalid");

    function checkBrackets(text) {
        let opened = 0;

        for (c of text) {
            if (c == '(') {
                opened++;
            } else if (c == ')') {
                if (opened == 0) {
                    return false;
                }
                opened--;
            }
        }
        return opened == 0;
    }
}