const run = () => {
    const src = document.querySelector('#str').value;
    const res = replaceSpaces(src);

    jsConsole.writeLine(res);

    function replaceSpaces(text) {
        return text.replace(/\s/g, '&nbsp;');
    }
}