const run = () => {
    let text = document.querySelector('#str').value;
    const operations = [{
            tag: "upcase",
            operator: s => s.toUpperCase()
        },
        {
            tag: "lowcase",
            operator: s => s.toLowerCase()
        },
        {
            tag: "mixcase",
            operator: s => {
                const res = [];
                for (i = 0; i < s.length; i++) {
                    if (i % 2 == 0) {
                        res.push(s[i].toLowerCase());
                    } else {
                        res.push(s[i].toUpperCase());
                    }
                }
                return res.join("");
            }
        }
    ]

    for (operation of operations) {
        text = replaceByTag(text, operation.tag, operation.operator);
    }

    jsConsole.writeLine(text);

    function replaceByTag(text, tag, operator) {
        const regex = new RegExp("\<" + tag + "\>([a-zA-Z]*?)\<\/" + tag + "\>", "g")
        const matches = text.matchAll(regex);
        for (match of matches) {
            text = text.replace(match[0], operator(match[1]));
        }
        return text;
    }
}