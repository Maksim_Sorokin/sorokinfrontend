const dimension = 4;
const key = "leaderboard";

function random(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

function generateTarget() {
    const target = [];
    target.push(random(1, 9));
    for (i = 1; i < dimension; i++) {
        let rand = null;
        while (rand == null || target.includes(rand)) {
            rand = random(0, 9);
        }
        target.push(rand);
    }
    return target;
}

function checkGuess(target, guess) {
    let rams = 0;
    let sheeps = 0;
    for (i = 0; i < dimension; i++) {
        const pos = target.indexOf(+guess[i]);
        if (pos < 0) {
            continue;
        }
        if (pos == i) {
            rams++;
        } else {
            sheeps++;
        }
    }
    return [rams, sheeps];
}

function saveScore(score, username) {
    let scores = [];
    const scoresJson = localStorage.getItem(key);
    if (scoresJson != undefined) {
        scores = JSON.parse(scoresJson);
    }
    scores.push({
        name: username,
        score: score
    });
    localStorage.setItem(key, JSON.stringify(scores));
}

function showLeaderBoard() {
    submitScore.style.display = "none";
    let scores = [];
    const scoresJson = localStorage.getItem(key);
    if (scoresJson != undefined) {
        scores = JSON.parse(scoresJson);
    }
    scores.sort(function(a, b) {
        return a.score - b.score;
    })
    let res = "User Results:";
    for (score of scores) {
        res = res + " <p>" + score.name + ": " + score.score + " moves </p>";
    }
    const div = document.createElement("div");
    div.innerHTML = res;
    document.body.appendChild(div);
}

const target = generateTarget();
console.log(target);
let attempts = 0;
const attemptsDiv = document.querySelector("#attempts");
const submitScoreDiv = document.querySelector("#submitScore");
const guesserDiv = document.querySelector("#guesser");
document.querySelector("#submitBtn").onclick = function() {
    const username = document.querySelector("#username").value;
    saveScore(attempts, username);
    showLeaderBoard();
}
document.querySelector("#tryGuess").onclick = function() {
    attempts++;
    const guess = document.querySelector('#xyzw').value.split('');
    const res = checkGuess(target, guess);
    if (res[0] == dimension) {
        attemptsDiv.style.display = "none";
        guesserDiv.style.display = "none";
        submitScore.style.display = "block";
    } else {
        const line = document.createElement("p");
        line.innerText = "You have " + res[0] + " ram(s) and " + res[1] + " sheep(s)";
        attemptsDiv.appendChild(line);
    }
}